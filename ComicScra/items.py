# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class CsdnItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()

class FzdmContentItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    url = scrapy.Field()
    image_url= scrapy.Field()
    page = scrapy.Field()
    book_name=scrapy.Field()


class ComicscraItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
