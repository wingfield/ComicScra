# -*- coding: utf-8 -*-
import scrapy
from scrapy import Selector

from ComicScra.items import FzdmContentItem


class FzdmSpider(scrapy.Spider):
    name = 'Fzdm'
    allowed_domains = ['manhua.fzdm.com']
    start_urls = ['http://manhua.fzdm.com/39/']

    def parse(self, response):
        sel = Selector(response)
        for link in sel.xpath("//div[@id='content']/li/a/@href").extract():
            nextUrl = response.url
            if nextUrl.endswith('/'):
                nextUrl = nextUrl+link
            else:
                nextUrl = nextUrl.rsplit('/',1)[0] + "/"+link
            request = scrapy.Request(nextUrl, callback=self.parse_item)
            yield request

    def parse_item(self, response):
        l = FzdmContentItem()
        l["image_url"] = response.xpath("//img[@id='mhpic']/@src").extract()[0]
        pages = response.xpath("//div[@class='navigation']/a[@id='mhona']")
        for pItem in pages:
            p = pItem.xpath('text()').extract()[0]
            plink = pItem.xpath('@href').extract()[0]
            p = p.strip()
            if p.startswith(u"第"):
                p = p.replace(u"第","").replace(u"页","")
                p = p.strip()
                l['page'] = "%03d"%int(p)
            if p == u"下一页":
                nextUrl = response.url
                if nextUrl.endswith('/'):
                    nextUrl = nextUrl + plink
                else:
                    nextUrl = nextUrl.rsplit('/', 1)[0] + "/" + plink
                request = scrapy.Request(nextUrl, callback=self.parse_item)
                yield request
        l["book_name"] = response.xpath("//div[@id='mh']/h1/text()").extract()[0].strip()
        l["url"] = response.url
        # l = ItemLoader(item=CsdnItem(), response=response)
        # l.add_xpath('title', "//h1/span[@class='link_title']/a/text()")
        # l.add_xpath('content', "//div[@id='article_content']/div[@class='markdown_views']/text()")
        # #l.add_xpath('image_urls', "//div[@id='picture']/p/img/@src", Identity())
        # l.add_value('url', response.url)
        yield l #.load_item()